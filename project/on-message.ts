import { Cowcow } from './cowcow';

import {
  Message,
  Room,
} from '../'

function delay(ms) {
  return new Promise(function (resolve) {
      setTimeout(resolve, ms);
  });
}

export async function onMessage(message: Message): Promise<void> {
  try {
    const room      = message.room()
    const sender    = message.from()
    const content   = message.content()

    if (room != null && room.topic() == 'wechat bot2'){
      console.log((room ? '[' + room.topic() + ']' : '')
      + '<' + sender.name() + '>'
      + ':' + message.toStringDigest(),
      )
    }

    // if (message.self() || room) {
    //   console.log('message is sent from myself, or inside a room.')
    //   return
    // }

    /********************************************
     *
     * 从下面开始修改vvvvvvvvvvvv
     *
     */

    if (room != null && room.topic() == 'wechat bot2'){
      if (content === 'start game') {
        Cowcow.updateUserPoint(7)
        await message.say('5 .. 4')
        await delay(1000)
        await message.say('3')
        await delay(1000)
        await message.say('2')
        await delay(1000)
        await message.say('1')
        await delay(1000)
        await message.say('starting...')
        return
      }
    }

    //if stranger talks to bot
    if (content === 'invite' && room == null) {
      await message.say('checking your group status')

      const myRoom = await Room.find({ topic: 'wechat bot2' })
      if (!myRoom) return

      if (myRoom.has(sender)) {
        await sender.say('hi ' + sender + ', you are already inside the group')
        return
      }

      await sender.say('ok, I will put you in game room!')
      await myRoom.add(sender)
      return

    } 

    /**
     *
     * 到这里结束修改^^^^^^^^^^^^
     *
     */
    /*********************************************/
  } catch (e) {
    console.error(e)
  }
}
