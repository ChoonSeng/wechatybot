import {
    config,
    Wechaty,
    log,
  }           from '../'
  
  import { onMessage }  from './on-message'
  import { onRoomJoin } from './on-room-join'

  var mysql      = require('mysql');
  var connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
    password : 'password',
    database : 'wechat',
    insecureAuth: true,
  });
//   var connection = mysql.createConnection({
//     host     : 'localhost',
//     user     : 'root',
//     password : '',
//     database : 'wechat',
//     insecureAuth: true,
//   });
  connection.connect();

  const welcome = `
  ---------------------------------
  ===== BOT INITILIZATION =========
  ---------------------------------
`
console.log(welcome)

//---------------------------------
//===== BOT INITILIZATION =========
//---------------------------------
var qrCode = 0
var qrURL = "Loading..."

var bot = Wechaty.instance({ profile: config.DEFAULT_PROFILE })
.on('scan', (url, code) => {
  if (!/201|200/.test(String(code))) {
    const loginUrl = url.replace(/\/qrcode\//, '/l/')
    require('qrcode-terminal').generate(loginUrl)
  }
  qrCode = code;
  qrURL = url;
  console.log(`${url}\n[${code}] Scan QR Code in above url to login: `)
})

.on('login'	  , function (this, user) {
  log.info('Bot', `${user.name()} logined`)
  this.say(`wechaty logined`)
})

.on('logout'	, function (this, user) {
    log.info('Bot', `${user.name()} logouted`)
    bot.quit();
  })
.on('error'   , error => log.info('Bot', 'error: %s', error))

.on('message',    onMessage)
.on('room-join',  onRoomJoin)
//---------------------------------
//===== Localhost Tunnel code =====
//---------------------------------
// var localtunnel = require('localtunnel');
// var tunnel = localtunnel(8080, function(err, tunnel) {
//    if (err) throw err
//    console.log(tunnel.url);
// });

//------------------------
//===== Express code =====
//------------------------
var express = require('express')
var session = require('express-session')
var path    = require("path")
var app = express()

app.use(session({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: true
}))
  
app.use(function (req, res, next) {
    if (!req.session.views) {
      //req.session.views = {}
    }
    if(!req.session.user){
        req.session.user = '';  
    }

    
    next()
})

//INDEX API
app.use(express.static('project/html'))            
app.get('/', function (req, res, next) {
    // console.log('user: ' + req.session.user)
    if (req.session.user == '') {
        //res.sendFile('/login.html');
        res.sendFile(path.join(__dirname+'/html/login.html'));        
    } else {
        //res.sendFile('player.html');
        res.sendFile(path.join(__dirname+'/html/player.html'));                
    }
  
})
//RETRIEVE QRCODE & URL API
app.get('/getQRCode',function(req,res){
    bot.init();
    res.write(qrURL)
    res.end();
})
/
//LOGOUT API
app.get('/logout',function(req,res,next){
    console.log('session destoryed')
    req.session.destroy();
    req.session = null;
    res.redirect('/');        
})

//LOGIN API

app.get('/login',function(req,res,next){
    connection.query('SELECT * FROM Admin WHERE USERNAME = "'+req.query['username']+'" AND PASSWORD = "'+req.query['password']+'"', function (error, results, fields) {
        if (error) throw error;
        if(results[0]==null){
            res.redirect('/'); 
        }else{
            //start the bot
            //bot.init()
            // for (var i in results) {
            //     console.log(results[i]);
            // }
            req.session.user = 'test';
            // console.log("login success: " + results[0]);
            res.redirect('/'); 
        }
        // console.log('Authentication in with results: ', JSON.parse(results));
      });
    
})

//STOP BOT API
app.get('/killbot',function(req,res){
    console.log('bot killed')
    qrURL = "Loading..."
    bot.logout()
    bot.quit()
    res.redirect('/');       
})

//RETRIEVE PLAYER API
app.get('/retrieveplayers',function(req,res){
    //if has session
    if (req.session.user == '') {
        res.redirect('/');   
        console.log("error insession")         
    } else {
        //return the players data
        type PlayerType = {
            id: number;
            name: string;
            point: number;
        }
        var arr: PlayerType[] = [] ;
        connection.query('SELECT * FROM player', function (error, results, fields) {
            if (error) throw error;
            for (var i in results) {
                var p = {id:results[i]['wechatID'], name:results[i]['username'], point:results[i]['points']};
                arr.push(p);
            }
            res.write(JSON.stringify(arr))
            res.end();
        });
    }      
})
//UPDATE PLAYER API
app.get('/updateplayer',function(req,res){
    var playid = req.query['id']
    var points = req.query['points']
    //if has session
    if (req.session.user == '') {
        res.redirect('/');            
    } else {
        connection.query('UPDATE Player SET points ="'+points+'" WHERE id = "'+playid+'"', function (error, results, fields) {
            if (error) throw error;
            res.redirect('/');
        });
    }        
})
//DELETE PLAYER API
app.get('/deleteplayer',function(req,res){
    var playid = req.query['id']

    //if has session
    if (req.session.user == '') {
        res.redirect('/');            
    } else {
        connection.query('DELETE Player WHERE id = "'+playid+'"', function (error, results, fields) {
            if (error) throw error;
            res.redirect('/');
        });
    }        
})


app.listen(8080, function () {
  console.log('Example app listening on port 8080!')
})