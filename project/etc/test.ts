/* tslint:disable:variable-name */
const QrcodeTerminal  = require('qrcode-terminal')
let link = 'test';

import {
  config,
  Wechaty,
  log,
}               from '../'

const welcome = `
| __        __        _           _
| \\ \\      / /__  ___| |__   __ _| |_ _   _
|  \\ \\ /\\ / / _ \\/ __| '_ \\ / _\` | __| | | |
|   \\ V  V /  __/ (__| | | | (_| | |_| |_| |
|    \\_/\\_/ \\___|\\___|_| |_|\\__,_|\\__|\\__, |
|                                     |___/

=============== Powered by Wechaty ===============
-------- https://github.com/chatie/wechaty --------

If you send me a 'ding', I will reply you a 'dong'!
__________________________________________________

Please wait... I'm trying to login in...

`
console.log(welcome)

var mysql      = require('mysql');
var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'newuser',
  password : 'password',
  database : 'wechat',
  insecureAuth: true,
});
connection.connect();




const bot = Wechaty.instance({ profile: config.DEFAULT_PROFILE })
bot
.on('logout'	, user => log.info('Bot', `${user.name()} logouted`))
.on('login'	  , user => {
  log.info('Bot', `${user.name()} logined`)
  bot.say('Wechaty login')
})
.on('error'   , e => {
  log.info('Bot', 'error: %s', e)
  bot.say('Wechaty error: ' + e.message)
})
.on('scan', (url, code) => {
  if (!/201|200/.test(String(code))) {
    const loginUrl = url.replace(/\/qrcode\//, '/l/')
    QrcodeTerminal.generate(loginUrl)
  }
  link = url;
  console.log(`${url}\n[${code}] Scan QR Code in above url to login: `)
})
bot.init()





var express = require('express');
var app = express();
app.get('/', function (req, res) {
  res.write('Hello World!'+ link)
  res.end();
})
app.listen(8080, function () {
  console.log('Example app listening on port 8080!')
})
app.get('/admin', function (req, res) {

  connection.query('SELECT * from admin', function (error, results, fields) {
    if (error) throw error;
    res.write('Users: ', results[0].username);
    res.end();
    console.log(results[0]);
  });

})


var localtunnel = require('localtunnel');
var tunnel = localtunnel(8080, function(err, tunnel) {
   if (err) throw err
   console.log(tunnel.url);
});

